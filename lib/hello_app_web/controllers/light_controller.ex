defmodule HelloAppWeb.LightController do
  use HelloAppWeb, :controller

  alias HelloApp.Devices
  alias HelloApp.Devices.Light

  action_fallback HelloAppWeb.FallbackController

  def index(conn, _params) do
    lights = Devices.list_lights()
    render(conn, :index, lights: lights)
  end

  def create(conn, %{"light" => light_params}) do
    with {:ok, %Light{} = light} <- Devices.create_light(light_params) do
      conn
      |> put_status(:created)
      |> put_resp_header("location", ~p"/api/v1/lights/#{light}")
      |> render(:show, light: light)
    end
  end

  def show(conn, %{"id" => id}) do
    light = Devices.get_light!(id)
    render(conn, :show, light: light)
  end

  def update(conn, %{"id" => id, "light" => light_params}) do
    light = Devices.get_light!(id)

    with {:ok, %Light{} = light} <- Devices.update_light(light, light_params) do
      render(conn, :show, light: light)
    end
  end

  def delete(conn, %{"id" => id}) do
    light = Devices.get_light!(id)

    with {:ok, %Light{}} <- Devices.delete_light(light) do
      send_resp(conn, :no_content, "")
    end
  end
end
