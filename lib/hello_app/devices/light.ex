defmodule HelloApp.Devices.Light do
  use Ecto.Schema
  import Ecto.Changeset

  schema "lights" do
    field :name, :string
    field :brightness, :integer

    timestamps(type: :utc_datetime)
  end

  @doc false
  def changeset(light, attrs) do
    light
    |> cast(attrs, [:name, :brightness])
    |> validate_required([:name, :brightness])
  end
end
