# HelloApp

Pointless little json api service to test and play with ArgoCD
set. See the accompanying
[hello_app_config](https://gitlab.com/eskil/hello_app_config) repo for
the argocd setup and other argo related steps (eg. pushing docker
images).

## Docker

The Dockerfile was created via `mix phx.gen.release --docker`, see [Phoenix Releases](https://hexdocs.pm/phoenix/releases.html)

See also [this logrocket blogpost](https://blog.logrocket.com/run-phoenix-application-docker/) for a walkthrough on running phoenix and docker.

The docker compose setup runs the service plus it's postgres db.

```
# Launch service in docker
docker compose up
```

## API

Here's some example curl statements that work against the docker
compose image (port `34000`). If you run the service locally or some
other way, you may need to use another port.

`mix phx.server` uses port 4000.

```
# Create a light
curl --header "Content-Type: application/json" --request POST --data '{"light": {"name":"Porch", "brightness":50}}' http://localhost:34000/api/lights

# Create a light
curl -H "Content-Type: application/json" -X POST -d '{"light": {"name":"Hall", "brightness":100}}' http://localhost:34000/api/lights

# Fetch all lights
curl -H "Content-Type: application/json" -X GET http://localhost:34000/api/lights | jq .

# Fetch second light
curl -H "Content-Type: application/json" -X GET http://localhost:34000/api/lights/2 | jq .
```

The full curl flags are only in the first use
* `-X / --request`
* `-H / --header`
* `-d / --data`
