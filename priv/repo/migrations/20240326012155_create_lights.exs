defmodule HelloApp.Repo.Migrations.CreateLights do
  use Ecto.Migration

  def change do
    create table(:lights) do
      add :name, :string
      add :brightness, :integer

      timestamps(type: :utc_datetime)
    end
  end
end
