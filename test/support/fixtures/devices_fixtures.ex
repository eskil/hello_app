defmodule HelloApp.DevicesFixtures do
  @moduledoc """
  This module defines test helpers for creating
  entities via the `HelloApp.Devices` context.
  """

  @doc """
  Generate a light.
  """
  def light_fixture(attrs \\ %{}) do
    {:ok, light} =
      attrs
      |> Enum.into(%{
        brightness: 42,
        name: "some name"
      })
      |> HelloApp.Devices.create_light()

    light
  end
end
