defmodule HelloApp.DevicesTest do
  use HelloApp.DataCase

  alias HelloApp.Devices

  describe "lights" do
    alias HelloApp.Devices.Light

    import HelloApp.DevicesFixtures

    @invalid_attrs %{name: nil, brightness: nil}

    test "list_lights/0 returns all lights" do
      light = light_fixture()
      assert Devices.list_lights() == [light]
    end

    test "get_light!/1 returns the light with given id" do
      light = light_fixture()
      assert Devices.get_light!(light.id) == light
    end

    test "create_light/1 with valid data creates a light" do
      valid_attrs = %{name: "some name", brightness: 42}

      assert {:ok, %Light{} = light} = Devices.create_light(valid_attrs)
      assert light.name == "some name"
      assert light.brightness == 42
    end

    test "create_light/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Devices.create_light(@invalid_attrs)
    end

    test "update_light/2 with valid data updates the light" do
      light = light_fixture()
      update_attrs = %{name: "some updated name", brightness: 43}

      assert {:ok, %Light{} = light} = Devices.update_light(light, update_attrs)
      assert light.name == "some updated name"
      assert light.brightness == 43
    end

    test "update_light/2 with invalid data returns error changeset" do
      light = light_fixture()
      assert {:error, %Ecto.Changeset{}} = Devices.update_light(light, @invalid_attrs)
      assert light == Devices.get_light!(light.id)
    end

    test "delete_light/1 deletes the light" do
      light = light_fixture()
      assert {:ok, %Light{}} = Devices.delete_light(light)
      assert_raise Ecto.NoResultsError, fn -> Devices.get_light!(light.id) end
    end

    test "change_light/1 returns a light changeset" do
      light = light_fixture()
      assert %Ecto.Changeset{} = Devices.change_light(light)
    end
  end
end
